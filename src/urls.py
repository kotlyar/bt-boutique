# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    (r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('src.main.urls')),
    url(r'^service/', include('src.services.urls')),
    url(r'^shop/', include('src.shop.urls')),
    # Страница обработки товара при добавлении в корзину
    url(r'^buy/(?P<product_slug>[-\w]+)/$', 'src.shop.views.buy'),
    # Просмотр корзины с товарами и услугами
    url(r'cart/', 'src.shop.views.cart_view'),
)


if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,}),)
    urlpatterns += staticfiles_urlpatterns()


from filebrowser.sites import site
urlpatterns += patterns('',
                        url(r'^admin/filebrowser/', include(site.urls)),
                        )