# -*- coding: utf-8 -*-
#!/usr/bin/env python
from django.db import models
from django.db.models import permalink
from mptt.models import MPTTModel, TreeForeignKey
from filebrowser.fields import FileBrowseField


class Category(MPTTModel):
    """Класс для категорий товаров"""
    name = models.CharField(u'Название', max_length=50, unique=True)
    slug = models.SlugField(u'Ссылка', max_length=50, unique=True,
                            help_text=u'Ссылка формируется из названия')
    # "Чистые" ссылки для продуктов формирующиеся из названия
    description = models.TextField(u'Описание')
    meta_keywords = models.CharField(u'Ключевые слова', max_length=255,
                                     help_text=u'Через запятую')
    # Разделенные запятыми теги для SEO оптимизации
    meta_description = models.CharField(u'Краткое описание', max_length=255,
                                        help_text=u'Используется для поисквой оптимизации')
    created_at = models.DateTimeField(u'Дата создание', auto_now_add=True)
    updated_at = models.DateTimeField(u'Дата обновления', auto_now=True)
    parent = TreeForeignKey('self', verbose_name=u'Родительская категория',
                            related_name='children', blank=True,
                            help_text=u'Родитлеьская категория для данной категории', null=True)

    class Meta:
        db_table = 'categories'
        ordering = ['-created_at']
        verbose_name_plural = u'Категории'

    def __unicode__(self):
        return self.name

    @permalink
    def get_absolute_url(self):
        """Генерация постоянных ссылок на категории"""
        return ('catalog_category', (), {'category_slug': self.slug})



class FeaturedProductManager(models.Manager):
    """Класс менеджер для вывода продвигаемых продуктов"""
    def get_query_set(self):
        return super(FeaturedProductManager, self).get_query_set().filter(is_featured=True)


class Product(models.Model):
    """Класс для товаров"""
    name = models.CharField(u'Название', max_length=255, unique=True)
    slug = models.SlugField(u'Ссылка', max_length=255, unique=True,
                            help_text=u'Уникальное значение для каждого товара. Формируется из названия.')
    brand = models.CharField(u'Производитель', max_length=50, blank=True)
    price = models.DecimalField(verbose_name=u'Стимость единицы товара', max_digits=9, decimal_places=2)
    old_price = models.DecimalField(max_digits=9, decimal_places=2,
                                    blank=True, default=0.00)
    is_featured = models.BooleanField(u'Отображать на главной', default=False) # Отображать на главной
    quantity = models.IntegerField(u'Quantity')
    description = models.TextField(u'Описание')
    meta_keywords = models.CharField(u'Ключевые слова', max_length=255,
                                     help_text=u'Через запятую')
    meta_description = models.CharField(u'Краткое описание', max_length=255,
                                        help_text=u'Используется для поисковой оптимизации')
    created_at = models.DateTimeField(u'Дата добавления', auto_now_add=True)
    updated_at = models.DateTimeField(u'Дата обновления', auto_now=True)
    #!buy_count = models.IntegerField(u'Количество продаж', default=0)
    categories = models.ManyToManyField(Category, verbose_name=u'Категории',
                                        help_text=u'Категории, к которым принадлежит продукт')

    objects = models.Manager()
    feautured = FeaturedProductManager()
    class Meta:
        db_table = 'products'
        ordering = ['-created_at']
        verbose_name_plural = u'Товары'

    def __unicode__(self):
        return self.name

    @permalink
    def get_absolute_url(self):
        """Генерация постоянных ссылок на товары"""
        return ('catalog_product', (), {'product_slug': self.slug})

    @property
    def sale_price(self):
        """
        Метод возвращает старую цену товара
        будет использоваться в шаблонах для отображения
        старой цены под текущей
        """
        if self.old_price > self.price:
            return True
        else:
            return False


class ProductImage(models.Model):
    """Изображения продуктов"""
    product = models.ForeignKey(Product, verbose_name=u'Товар')
    image = FileBrowseField(directory=u'ProductPhotos/', verbose_name=u'Фото', max_length=250, blank=True, null=True)
    description = models.CharField(verbose_name=u'Подпись', max_length=250, blank=True)
    default = models.BooleanField(u'Главное фото', default=False)

    class Meta:
        db_table = 'product_images'
