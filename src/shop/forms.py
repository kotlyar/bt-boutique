# -*- coding: utf-8 -*-

from django import forms
from src.shop.cart.models import Client

class OrderForm(forms.ModelForm):

    class Meta:
        model = Client
        fields = ('name', 'email', 'phone', 'other')
