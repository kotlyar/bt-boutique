# -*- coding: utf-8 -*-
#!/usr/bin/env python
from django.contrib import admin
from src.shop.models import Product, Category, ProductImage
from mptt.admin import MPTTModelAdmin


class ProductImageAdmin(admin.TabularInline):
    """Добавление изображений продукта"""
    model = ProductImage
    extra = 1



class ProductAdmin(admin.ModelAdmin):
    """
    Управление товарами
    Как будут отображаться поля товаров в разделе администрирования
    """
    list_display = ('name', 'price', 'old_price', 'created_at', 'updated_at',)
    list_display_links = ('name',)
    list_per_page = 50
    ordering = ['-created_at']
    inlines = [ProductImageAdmin,]
    search_field = ['name', 'description', 'meta_keywords', 'meta_description']
    # exclude = ('created_at', 'updated_at',)
    readonly_fields = ('created_at', 'updated_at',)
    # имя продукта для генерации чистой ссылки
    prepopulated_fields = {'slug': ('name',)}


class CategoryAdmin(MPTTModelAdmin):
    """
    Управление категориями
    Как будут отображаться поля категорий в разделе администрирования
    """
    list_display = ('name', 'created_at', 'updated_at',)
    list_display_links = ('name',)
    list_per_page = 20
    ordering = ['name']
    search_fields = ['name', 'description', 'meta_keywords', 'meta_description']
    readonly_fields = ('created_at', 'updated_at',)
    # exclude = ('created_at', 'updated_at',)
    prepopulated_fields = {'slug': ('name',)}
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        field = super(CategoryAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
        if db_field.name == 'parent':
            field.choices = [('','---------')]
            for rubric in Category.objects.all():
                field.choices.append((rubric.pk, '+--'*(rubric.level) + rubric.name))
        return field



# Регистрирация моделей в админке
admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)