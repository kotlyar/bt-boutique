# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

urlpatterns = patterns('src.shop.views',
    # Главная страница
    url(r'^$', 'index_view', name='catalog_home'),
    # Просмотр категории
    url(r'^category/(?P<category_slug>[-\w]+)/$', 'category_view', name='catalog_category'),
    # Просмотр товара
    url(r'^product/(?P<product_slug>[-\w]+)/$', 'product_view', name='catalog_product'),
)