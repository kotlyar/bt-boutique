# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.http import Http404, HttpResponseRedirect

from src.decorators import render_to
from .models import Product, Category
from src.services.hotels.models import HotelRoom
from src.shop.cart.models import CartItem

from cart import cart


@render_to('shop/index.html')
def index_view(request):
    sales = []
    for product in Product.objects.all():
        if product.sale_price:
            sales.append(product)
    other = Product.feautured.all()
    return {'sales': sales, 'other': other}


def create_road(cat):
    import copy
    node_list = []
    node_list.append(cat)
    parent = cat.parent
    while parent:
        node_list.append(copy.deepcopy(parent))
        parent = parent.parent
    road = u'<a href="/shop/">Магазин</a>'
    for nod in reversed(node_list):
        road += u' > <a href="/shop/category/%s/">%s</a>' % (unicode(nod.slug), unicode(nod.name))
    return road


@render_to('shop/category_view.html')
def category_view(request, category_slug):
    c = get_object_or_404(Category.objects, slug = category_slug) #Берем категорию
    ch = c.get_descendants() # ВСЕ дочерние категории
    child = [i for i in ch if i.parent.name == c.name] #Только те дочерние категории, родитель которых - данная категория
    products = c.product_set.all()
    return {'category': c, 'children_cat': child, 'products': products, 'road': create_road(c)}


@render_to('shop/product_view.html')
def product_view(request, product_slug):
    product = get_object_or_404(Product.objects, slug = product_slug)
    return {'product': product, 'road': create_road(product.categories.all()[0])}


@render_to('shop/buy.html')
def buy(request, product_slug):
    if request.method == 'POST':
        if Product.objects.filter(slug=product_slug):
            cart.product_add_to_cart(request)
        elif HotelRoom.objects.filter(slug=product_slug):
            cart.hotel_add_to_cart(request)
        messages.success(request, u'Заказ добавлен в корзину')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        raise Http404


from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.template import RequestContext
from .forms import OrderForm
from cart.models import Client
@csrf_protect
def cart_view(request):
    """Представление для отображения корзины"""
    page_title = u'Просмотр корзины'
    if request.method == 'POST':
        postdata = request.POST.copy()
        if postdata.has_key('remove'):
            cart.remove_from_cart(request)
        if postdata.has_key('update'):
            cart.update_cart(request)
        if postdata.has_key('checkout'):
            form = OrderForm(request.POST)
            if form.is_valid():
                tmpcart = CartItem.objects.get(cart_id=cart.get_cart_id(request))
                cl = Client(name=request.POST['name'], email=request.POST['email'], phone=request.POST['phone'], other=request.POST['other'])
                cl.cart = tmpcart
                cl.save()
                tmpcart.order = True
                tmpcart.save()
                messages.success(request,u'Спасибо за Ваш заказ. Мы свяжемся с Вами в ближайшее время.')
                cart.empty_cart(request)
                return HttpResponseRedirect('/')
            else:
                messages.error(request, u'Неверно введенные данные.')
    form = OrderForm()
    cart_items = cart.get_cart_items(request)
    products = cart_items['products']
    hotels = cart_items['hotels']
    cart_subtotal = cart.cart_subtotal(request)
    return render_to_response('shop/cart.html', locals(),
                              context_instance=RequestContext(request))
