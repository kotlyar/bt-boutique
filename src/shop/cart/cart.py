# -*- coding: utf-8 -*-
#!/usr/bin/env python
import decimal
import random
import datetime

from django.shortcuts import get_object_or_404
from django.contrib import messages

from src.shop.models import Product
from src.services.hotels.models import HotelRoom
from src.shop.cart.models import CartItem, ProductItem, HotelItem


CART_ID_SESSION_KEY = 'cart_id'

def _cart_id(request):
    """
      Получение id корзины из cookies для пользователя,
      или установка новых cookies если не существуют
      _модификатор для видимости в пределах модуля
      """
    if request.session.get(CART_ID_SESSION_KEY, '') == '':
        request.session[CART_ID_SESSION_KEY] = _generate_cart_id()
    return request.session[CART_ID_SESSION_KEY]

def _generate_cart_id():
    """Генерация уникального id корзины который будет хранится в cookies"""
    cart_id = ''
    characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()'
    cart_id_length = 50
    for y in range(cart_id_length):
        cart_id += characters[random.randint(0, len(characters) - 1)]
    return cart_id

def get_cart_id(request):
    if request.session.get(CART_ID_SESSION_KEY, '') == '':
        return None
    else:
        return request.session[CART_ID_SESSION_KEY]

def get_cart_items(request):
    """Получение всех товаров для текущей корзины"""
    try:
        tmp = CartItem.objects.get(cart_id=_cart_id(request))
    except:
        return {'products': [], 'hotels': []}
    return {'products': tmp.productitem_set.all(), 'hotels': tmp.hotelitem_set.all()}


def product_add_to_cart(request):
    postdata = request.POST.copy()
    product_slug = postdata.get('product_slug', '')
    quantity = postdata.get('quantity', 1)
    p = get_object_or_404(Product, slug=product_slug)
    cart_products = get_cart_items(request)
    cart_products = cart_products['products']
    product_in_cart = False
    for cart_item in cart_products:
        if cart_item.product.id == p.id:
            cart_item.augment_quantity(quantity)
            product_in_cart = True
    if not product_in_cart:
        try:
            ci = CartItem.objects.get(cart_id=_cart_id(request))
        except:
            ci = CartItem()
            ci.cart_id = _cart_id(request)
            ci.save()
        pi = ProductItem()
        pi.cart = ci
        pi.quantity = quantity
        pi.product = p
        pi.save()

def hotel_add_to_cart(request):
    postdata = request.POST.copy()
    product_slug = postdata.get('product_slug', '')
    quantity = postdata.get('quantity', 1)
    p = get_object_or_404(HotelRoom, slug=product_slug)
    cart_products = get_cart_items(request)
    cart_products = cart_products['hotels']
    product_in_cart = False
    for cart_item in cart_products:
        if cart_item.product.id == p.id:
            cart_item.augment_quantity(quantity)
            product_in_cart = True
    if not product_in_cart:
        try:
            ci = CartItem.objects.get(cart_id=_cart_id(request))
        except:
            ci = CartItem()
            ci.cart_id = _cart_id(request)
            ci.save()
        hi = HotelItem()
        hi.cart = ci
        hi.quantity = quantity
        hi.product = p
        hi.date_on = datetime.datetime.strptime(unicode(request.POST['date_on']), '%d.%m.%Y')
        hi.date_off = datetime.datetime.strptime(unicode(request.POST['date_off']), '%d.%m.%Y')
        hi.save()

def cart_distinct_item_count(request):
    """Возвращает общее количество товаров в корзине"""
    tmp = get_cart_items(request)
    count = 0
    if tmp['products']:
        count += len(list(tmp['products']))
    if tmp['hotels']:
        count += len(list(tmp['hotels']))
    return count

def get_single_item(request, sl):
    """Получаем конкретный товар в корзине"""
    try:
        cart = CartItem.objects.get(cart_id=_cart_id(request))
    except:
        return None
    pr = cart.productitem_set.all()
    for p in pr:
        if p.product.slug == sl:
            return p
    hot = cart.hotelitem_set.all()
    for h in hot:
        if h.product.slug == sl:
            return h
    return None


def update_cart(request):
    """Обновляет количество отдельного товара"""
    postdata = request.POST.copy()
    sl = postdata['product_slug']
    quantity = postdata['quantity']
    cart_item = get_single_item(request, sl)
    if cart_item:
        if quantity and int(quantity) > 0:
            cart_item.quantity = int(quantity)
            cart_item.save()
        else:
            messages.error(request, u'Недопустимое значение')
    else:
        messages.error(request, u'Товар не найден')

    #TODO: добавить предупреждение
    #    remove_from_cart(request)

def remove_from_cart(request):
    """Удаляет выбранный товар из корзины"""
    postdata = request.POST.copy()
    sl = postdata['product_slug']
    cart_item = get_single_item(request, sl)
    if cart_item:
        cart_item.delete()

def cart_subtotal(request):
    """Получение суммарной стоимости всех товаров"""
    cart_total = decimal.Decimal('0.00')
    cart_products = get_cart_items(request)
    for cart_item in cart_products['products']:
        cart_total += cart_item.product.price * cart_item.quantity
    for cart_item in cart_products['hotels']:
        cart_total += cart_item.product.price * cart_item.quantity
    return cart_total

def is_empty(request):
    """Если корзина пустая возвращаем True"""
    return cart_distinct_item_count(request) == 0

def empty_cart(request):
    """Очищает корзину покупателя"""
    request.session[CART_ID_SESSION_KEY] = ''