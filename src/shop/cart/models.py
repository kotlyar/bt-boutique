# -*- coding: utf-8 -*-

import decimal
from django.db import models
from src.shop.models import Product
from src.services.hotels.models import HotelRoom


class CartItem(models.Model):
    cart_id = models.CharField(max_length=50)
    date_added = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата')
    order = models.BooleanField(default=False, verbose_name=u'Оформлено')
    complit = models.BooleanField(default=False, verbose_name=u'Выполнено')

    class Meta:
        ordering = ['date_added']
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'

    def __unicode__(self):
        return u'Корзина ' + unicode(self.date_added)


class ProductItem(models.Model):
    """
    Класс для товаров в корзине, хранит данные о том
    какой товар в корзине и его количество
    """
    cart = models.ForeignKey(CartItem)
    product = models.ForeignKey(Product, unique=False, verbose_name=u'Товар')
    quantity = models.IntegerField(default=1,verbose_name=u'Количество')

    class Meta:
        verbose_name = u'Заказанный товар'
        verbose_name_plural = u'Заказанные товары'

    @property
    def total(self):
        """Метод для подсчета суммы, цена товара * кол-во"""
        return decimal.Decimal(self.quantity * float(self.product.price))

    @property
    def name(self):
        """Получение названия товара в корзине"""
        return self.product.name

    @property
    def price(self):
        """Получение цены товара в корзине"""
        return self.product.price

    def get_absolute_url(self):
        """Получение абсолютной ссылки на товар"""
        return self.product.get_absolute_url()

    def augment_quantity(self, quantity):
        """Изменение количества товара в корзине"""
        if quantity:
            self.quantity = self.quantity + int(quantity)
            self.save()


class HotelItem(models.Model):
    cart = models.ForeignKey(CartItem)
    product = models.ForeignKey(HotelRoom, unique=False, verbose_name=u'Название')
    quantity = models.IntegerField(verbose_name=u'Количество', default=1)
    date_on = models.DateField(verbose_name=u'Дата заезда')
    date_off = models.DateField(verbose_name=u'Дата выезда')

    class Meta:
        verbose_name = u'Гостиничный номер'
        verbose_name_plural = u'Гостиничные номера'

    @property
    def total(self):
        """Метод для подсчета суммы, цена товара * кол-во"""
        return decimal.Decimal(self.quantity * float(self.product.price))

    @property
    def name(self):
        """Получение названия товара в корзине"""
        return self.product.title

    @property
    def price(self):
        """Получение цены товара в корзине"""
        return self.product.price

    def get_absolute_url(self):
        """Получение абсолютной ссылки на товар"""
        return self.product.get_absolute_url()

    def augment_quantity(self, quantity):
        """Изменение количества товара в корзине"""
        if quantity:
            self.quantity = self.quantity + int(quantity)
            self.save()


class Client(models.Model):
    """Данные о заказчике"""
    name = models.CharField(verbose_name=u'Имя', max_length=100)
    phone = models.CharField(verbose_name=u'Контактный телефон', max_length=20)
    email = models.EmailField(verbose_name=u'Электронная почта')
    other = models.TextField(verbose_name=u'Комментарии к заказу', blank=True)
    cart = models.ForeignKey(CartItem)

    class Meta:
        verbose_name = u'Клиент'
        verbose_name_plural = u'Клиент'