from django.contrib import admin

from .models import CartItem, ProductItem, HotelItem
from src.shop.cart.models import Client

class ProductItemAdmin(admin.TabularInline):
    model = ProductItem
    extra = 0


class HotelItemAdmin(admin.TabularInline):
    model = HotelItem
    extra = 0

class ClientAdmin(admin.TabularInline):
    model = Client
    extra = 0

class CartItemAdmin(admin.ModelAdmin):
    model = CartItem
    list_display = ('date_added', 'order', 'complit')
    ordering = ('-order', 'date_added', '-complit')
    list_filter = ('order', 'complit', 'date_added')
    inlines = [ClientAdmin, ProductItemAdmin, HotelItemAdmin]


admin.site.register(CartItem, CartItemAdmin)