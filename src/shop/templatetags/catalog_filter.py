# -*- coding: utf-8 -*-

from django import template

register = template.Library()


@register.filter
def default_image(value):
    try:
        photo = value.productimagde_set.get(default=True)
        return photo.image
    except:
        try:
            photo = value.productimage_set.all()[0]
            return photo.image
        except:
            return 'img/no_foto.jpg'
