# -*- coding: utf-8 -*-

from django import template

from src.shop.models import Category


register = template.Library()


@register.inclusion_tag("catalog/category_list.html")
def categories_tree():
    """Возвращает дерево категорий"""
    return {'nodes': Category.objects.all()}


@register.inclusion_tag("catalog/cat_block.html")
def products_block(title,products,toby):
    """Товары передаются в виде списка списков по 3 товара"""
    pr_list = []
    pr_count = len(products)
    list_count = pr_count / 3 if not pr_count % 3 else (pr_count / 3 + 1)
    for i in range(list_count):
        pr_list.append(products[i*3:(i+1)*3])
    return {'title': title, 'products': pr_list, 'toby': toby}


@register.inclusion_tag("catalog/product_preview.html")
def product_preview(product):
    return {'product': product}
