# -*- coding: utf-8 -*-

from src.shop.cart import cart

def cart_count(request):
    cart_idd = cart.get_cart_id(request)
    if cart_idd and not cart.is_empty(request):
        c = cart.cart_distinct_item_count(request)
    else:
        c = 0
    return {'cart_item_count': c}