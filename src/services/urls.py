# -*- coding: utf-8 -*-

from django.conf.urls import url, patterns, include


urlpatterns = patterns('src.services.views',
    url(r'^$', 'index', name='index'),
    url(r'^avia/', include('src.services.avia.urls')),
    url(r'^hotels/', include('src.services.hotels.urls')),
)