# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import State, City
from hotels.models import Hotel, HotelRoom, Photos, HotelOrder
from django.conf import settings

#State and City register
class CityInline(admin.TabularInline):
    model = City


class StateAdmin(admin.ModelAdmin):
    model = State
    inlines = [
        CityInline
    ]

admin.site.register(State, StateAdmin)


#Hotels in Catalog
class HotelRoomInline(admin.TabularInline):
    model = HotelRoom
    extra = 1


class PhotosInline(admin.TabularInline):
    model = Photos
    extra = 1


class HotelAdmin(admin.ModelAdmin):
    model = Hotel
    list_display = ('title', 'city', 'address', 'starcount')
    inlines = [HotelRoomInline, PhotosInline]

    class Media:
        js = [
            settings.MEDIA_URL + 'grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            settings.MEDIA_URL + 'grappelli/tinymce_setup/tinymce_setup.js',
            ]

admin.site.register(Hotel, HotelAdmin)


#Hotel orders if not room in catalog
class HotelOrderAdmin(admin.ModelAdmin):
    model = HotelOrder
    list_display = ('email', 'phone', 'date_order', 'city', 'complit')
    list_filter = ('complit','date_order')


admin.site.register(HotelOrder, HotelOrderAdmin)

