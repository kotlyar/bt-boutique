# -*- coding: utf-8 -*-

from src.decorators import render_to

@render_to('services/index.html')
def index(request):
    return {}