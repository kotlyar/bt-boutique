# -*- coding: utf-8 -*-

from src.decorators import render_to

@render_to('services/avia/index.html')
def index(request):
    return {}