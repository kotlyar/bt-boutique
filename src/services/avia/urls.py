# -*- coding: utf-8 -*-

from django.conf.urls import url, patterns


urlpatterns = patterns('src.services.avia.views',
    url(r'^$', 'index', name='index'),
)