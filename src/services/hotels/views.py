# -*- coding: utf-8 -*-

from django.http import Http404, HttpResponseRedirect
from django.contrib import messages

from src.decorators import render_to
from .models import Hotel, HotelRoom
from src.services.models import City
from .forms import HotelOrderForm


import django_filters
from django_filters.widgets import LinkWidget

class HotelRoomFilter(django_filters.FilterSet):
    price = django_filters.RangeFilter(label=u'Стоимость')
    hotel__starcount = django_filters.AllValuesFilter(label=u'Количество звезд', widget=LinkWidget)
    class Meta:
        model = HotelRoom
        fields = ['price','hotel__city','hotel__title','hotel__starcount']

    def __init__(self, *args, **kwargs):
        super(HotelRoomFilter, self).__init__(*args, **kwargs)
        self.filters['hotel__city'].extra.update({'empty_label': u'Любой'})


def m_cl(k, req):
    if (k in req.GET.keys()) and (req.GET[k]):
        return True
    else:
        return False


@render_to('services/hotels/index.html')
def index(request):
    roomf = HotelRoomFilter(request.GET, queryset=HotelRoom.objects.all())
    hotels = []
    for room in roomf:
        if not room.hotel in hotels:
            hotels.append(room.hotel)

    titles = u'Гостиницы'
    if m_cl('hotel__title', request):
        titles += u' c названием <span>'+request.GET['hotel__title']+u'</span>'
    if m_cl('hotel__starcount', request):
        count = request.GET['hotel__starcount']
        if count=='1':
            titles += u' <span>1 звезда</span>'
        elif count=='0' or count==5:
            titles += u' <span>'+count+u' звезд</span>'
        elif count in ['2','3','4']:
            titles += u' <span>'+count+u' звезды</span>'
    if m_cl('hotel__city', request):
        try:
            city = City.objects.get(pk=request.GET['hotel__city'])
            titles += u' в городе <span>' + city.title + u'</span>'
        except:
            pass
    if m_cl('price_0',request) and m_cl('price_1',request):
        titles += u' с ценой от <span>'+request.GET['price_0'] + u'</span> до <span>' + request.GET['price_1'] + u'</span> р. за ночь'

    if request.method == 'POST':
        form = HotelOrderForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, u'Спасибо за Ваш заказ.')
            return HttpResponseRedirect(request.path)
        else:
            messages.error(request, u'Пожалуйста, исправьте ошибки и отправьте форму еще раз')
    form = HotelOrderForm()
    return {'room_filter': roomf, 'hotels': hotels, 'titles': titles, 'form': form}


@render_to('services/hotels/hotel_view.html')
def hotel_view(request, hotel_pk):
    try:
        hotel = Hotel.objects.get(pk=hotel_pk)
    except:
        raise Http404

    return {'hotel': hotel,}