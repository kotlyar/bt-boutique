# -*- coding: utf-8 -*-

from django.conf.urls import url, patterns


urlpatterns = patterns('src.services.hotels.views',
    url(r'^$', 'index', name='index'),
    url(r'(?P<hotel_pk>\d+)/$', 'hotel_view', name='hotel_view'),
)