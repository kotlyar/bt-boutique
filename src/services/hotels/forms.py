# -*- coding: utf-8 -*-

from django import forms
from .models import HotelOrder


class HotelOrderForm(forms.ModelForm):

    class Meta:
        model = HotelOrder
        fields = ('email', 'phone', 'city', 'title', 'starcount', 'date_on', 'date_off', 'people_count', 'content')
