# -*- coding: utf-8 -*-
from __builtin__ import unicode

from django.db import models
from django.db.models import permalink
from filebrowser.fields import FileBrowseField
from src.services.models import City


class Hotel(models.Model):
    """
        Каталог готелей. Содержит основную информацию о готеле - адрес, количесвто звезд, описание
    """
    city = models.ForeignKey(City, verbose_name=u'Город')
    title = models.CharField(verbose_name=u'Название гостиницы', max_length=250, blank=True)
    address = models.CharField(verbose_name=u'Адрес', help_text=u'Без указания города', max_length=250, blank=True)
    starcount = models.IntegerField(verbose_name=u'Количество звезд', default=0)
    content = models.TextField(verbose_name=u'Описание', blank=True)
    otherservices = models.TextField(verbose_name=u'Дополнительные услуги', help_text=u'Wi-Fi, завтрак, прачечная и т.д', blank=True)

    class Meta:
        verbose_name = u'Гостиница'
        verbose_name_plural = u'Гостиницы'
        ordering = ['city', 'title']

    def __unicode__(self):
        return u'"%s" в городе %s, %s' % (self.title, self.city.title, self.city.state.title)



class HotelRoom(models.Model):
    """
        Готельные комнаты. Для каждой гостиницы может быть несколько комнат - описание, стоимость за ночь
    """
    hotel = models.ForeignKey(Hotel, verbose_name=u'Гостиница')
    title = models.CharField(verbose_name=u'Название', max_length=250)
    slug = models.SlugField(u'Ссылка', max_length=255, unique=True,
                            help_text=u'Уникальное значение. Формируется из названия.')
    price = models.DecimalField(verbose_name=u'Стоимость за ночь', max_digits=9, decimal_places=2)
    bed = models.CharField(verbose_name=u'Расположение кроватей', max_length=250, blank=True)
    description = models.CharField(verbose_name=u'Описание', max_length=250, blank=True)

    class Meta:
        verbose_name = u'Готельный номер'
        verbose_name_plural = u'Готельные номера'

    def __unicode__(self):
        return self.title + u' - ' + str(self.price) + u'р./ночь'

    @permalink
    def get_absolute_url(self):
        return ('hotel_view', (), {'hotel_pk': self.hotel.pk})


class Photos(models.Model):
    """
        Фотографии гостиницы, включая фото комнат (не отдельно).
        description выводится под фотографией
    """
    hotel = models.ForeignKey(Hotel, verbose_name=u'Гостиница')
    image = FileBrowseField(directory=u'HotelPhotos/', verbose_name=u'Фото', max_length=250, blank=True, null=True)
    description = models.CharField(verbose_name=u'Подпись', max_length=250, blank=True)

    class Meta:
        verbose_name = u'Фото гостиницы'
        verbose_name_plural = u'Фото гостиницы'


# class HotelRoomOrder(models.Model):
#     """
#         Модель для заказа гостиничных номеров из каталога
#     """
#     room = models.ForeignKey(HotelRoom, verbose_name=u'Гостиничный номер')
#     count = models.IntegerField(verbose_name=u'Количество номеров', default=0)
#     date_on = models.DateField(verbose_name=u'Дата заезда')
#     date_off = models.DateField(verbose_name=u'Дата выезда')
#     date_order = models.DateField(verbose_name=u'Дата заказа', auto_now_add=True)
#     phone1 = models.CharField(verbose_name=u'Номер мобильного телефона',max_length=30)
#     phone2 = models.CharField(verbose_name=u'Дополнительный номер телефона', max_length=30, blank=True)
#     otherinfo = models.TextField(verbose_name=u'Дополнительная информация', blank=True)
#     complit = models.BooleanField(verbose_name=u'Выполнено', default=False)
#
#     class Meta:
#         verbose_name=u'Заказ гостиничного номера из каталога'
#         verbose_name_plural=u'Заказы гостиничных номеров из каталога'
#         ordering = ['complit', 'date_order', 'date_on']
#
#     def __unicode__(self):
#         return unicode(self.date_order) + ' - ' + self.room.title


class HotelOrder(models.Model):
    email = models.EmailField(verbose_name=u'Электронная почта')
    phone = models.CharField(verbose_name=u'Контактный телефон',max_length=30)
    city = models.CharField(verbose_name=u'Город', max_length=250)
    title = models.CharField(verbose_name=u'Название гостиницы', max_length=250, blank=True)
    starcount = models.CharField(verbose_name=u'Количество звезд', max_length=1, default=0)
    date_order = models.DateField(verbose_name=u'Дата заказа', auto_now_add=True)
    date_on = models.DateField(verbose_name=u'Дата поселения')
    date_off = models.DateField(verbose_name=u'Дата съезда')
    people_count = models.IntegerField(verbose_name=u'Количество человек', blank=True)
    content = models.TextField(verbose_name=u'Дополнительные пожелания', blank=True)
    complit = models.BooleanField(verbose_name=u'Выполнено', default=False)

    class Meta:
        verbose_name = u'Заказ гостиницы не из каталога'
        verbose_name_plural = u'Заказы гостиниц не из каталога'
        ordering = ['complit', 'date_order', 'date_on']

    def all_price(self):
        pass

