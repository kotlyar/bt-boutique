# -*- coding: utf-8 -*-

from django.db import models

class State(models.Model):
    title = models.CharField(verbose_name=u'Страна/Регион', max_length=250)

    class Meta:
        verbose_name = u'Страна'
        verbose_name_plural = u'Страны'
        ordering=['title',]

    def __unicode__(self):
        return self.title


class City(models.Model):
    state = models.ForeignKey(State, verbose_name=u'Страна')
    title = models.CharField(verbose_name=u'Город', max_length=250)

    class Meta:
        verbose_name = u'Город'
        verbose_name_plural = u'Города'
        ordering=['title',]

    def __unicode__(self):
        return self.title
