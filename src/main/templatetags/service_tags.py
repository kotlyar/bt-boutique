# -*- coding: utf-8 -*-

from django import template

register = template.Library()

@register.filter
def image(value):
    return value.image


@register.filter
def as_list(value):
    return range(value)
