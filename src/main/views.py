# -*- coding: utf-8 -*-

from django.views.generic import DetailView, ListView
from src.main.models import News
from src.decorators import  render_to

class NewsDetail(DetailView):
    model = News

news_detail = NewsDetail.as_view()

class NewsList(ListView):
    model = News
    paginate_by = 10

news = NewsList.as_view()


@render_to('main/index.html')
def index(request):
    return {}


@render_to('main/static_page.html')
def staticpage(request, content):
    return {'content': content}