    # -*- coding: utf-8 -*-

from django.db import models


class News(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'Заголовок')
    description = models.TextField(blank=True, verbose_name=u'Краткое описание')
    content = models.TextField(blank=True, verbose_name=u'Содержимое')
    created_at = models.DateField(auto_now_add=True, verbose_name=u'Дата создания')

    class Meta:
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return '/news/%s/' % self.pk

class StaticPage(models.Model):
    title = models.CharField(verbose_name=u'Заголовок', max_length=250)
    url = models.CharField(verbose_name=u'Ссылка', max_length=250)
    content = models.TextField(verbose_name=u'Содержимое')

    class Meta:
        verbose_name = u'Статическая страница'
        verbose_name_plural = u'Статические страницы'

    def __unicode__(self):
        return self.title
