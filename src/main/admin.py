# -*- coding: utf-8 -*-

import os
from django.conf import settings
from django.contrib import admin
from src.main.models import News, StaticPage


class NewsAdmin(admin.ModelAdmin):
    class Media:
        js = [
            os.path.join(settings.STATIC_URL, 'grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js'),
            os.path.join(settings.STATIC_URL, 'grappelli/tinymce_setup/tinymce_setup.js'),
            ]


    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(NewsAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'description':
            field.widget.attrs['class'] = 'mceNoEditor ' + field.widget.attrs.get('class', '').replace('vLargeTextField', 'vTextField')
        return field


class StaticPageAdmin(admin.ModelAdmin):
    model = StaticPage
    list_display = ('title', 'url')


admin.site.register(News, NewsAdmin)
admin.site.register(StaticPage, StaticPageAdmin)