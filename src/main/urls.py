from django.conf.urls import patterns, url, include
from src.main.models import StaticPage

urlpatterns = patterns('src.main.views',
    url(r'^$', 'index', name='index'),
    url(r'^news/$', 'news', name='news'),
    url(r'^news/(?P<pk>\d+)/$', 'news_detail', name='news_detail'),
)

for page in StaticPage.objects.all():
    urlpatterns += patterns('src.main.views',
        url(r'%s' % page.url, 'staticpage', {'content': page.content}, name='staticpage')
    )